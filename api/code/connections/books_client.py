from abc import ABC, abstractmethod


class LibraryClient(ABC):

    @abstractmethod
    def get_by_isbn(self, isbn):
        pass

    @abstractmethod
    def get_by_generic_query(self, **query):
        pass

    @abstractmethod
    def get_by_external_id(self, client_id):
        pass
