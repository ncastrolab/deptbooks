import os
from abc import ABC
from urllib.parse import urlencode

import httpx

from ..connections.books_client import LibraryClient


class OpenLibraryClient(LibraryClient, ABC):

    def __init__(self):
        self.server_host = os.getenv("BOOKS_REPOSITORY_URL")

    def get_by_isbn(self, isbn):
        with httpx.Client() as client:
            isb_search_response = client.get(
                f"{self.server_host}/isbn/{isbn}.json", follow_redirects=True
            )
            isb_search_response.raise_for_status()
            return isb_search_response.json()

    def get_by_generic_query(self, **query):
        with httpx.Client() as client:
            print(query)
            response = client.get(f"{self.server_host}/search.json/?{urlencode(query)}", follow_redirects=True)
            response.raise_for_status()
            return response.json()

    def get_by_external_id(self, client_id):
        with httpx.Client() as client:
            ol_id_response = client.get(
                f"{self.server_host}/books/{client_id}.json", follow_redirects=True
            )
            ol_id_response.raise_for_status()
            return ol_id_response.json()
