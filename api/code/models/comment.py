import uuid

from pydantic import Field, BaseModel


class CommentSave(BaseModel):
    text: str
    book_id: str


class CommentSaved(BaseModel):
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
    text: str
    book_id: str


class CommentUpdate(BaseModel):
    text: str
