from __future__ import annotations

import uuid
from typing import List, Any

from pydantic import BaseModel, Field


class Identifiers(BaseModel):
    goodreads: List[str]
    librarything: List[str]


class Author(BaseModel):
    key: str


class Language(BaseModel):
    key: str


class Type(BaseModel):
    key: str


class FirstSentence(BaseModel):
    type: str
    value: str


class Work(BaseModel):
    key: str


class Created(BaseModel):
    type: str
    value: str


class LastModified(BaseModel):
    type: str
    value: str


class OLBook(BaseModel):
    identifiers: Identifiers
    title: str
    authors: List[Author]
    publish_date: str
    publishers: List[str]
    isbn_10: List[str]
    covers: List[int]
    ocaid: str
    contributions: List[str]
    languages: List[Language]
    source_records: List[str]
    isbn_13: List[str]
    local_id: List[str]
    type: Type
    first_sentence: FirstSentence
    key: str
    number_of_pages: int
    works: List[Work]
    latest_revision: int
    revision: int
    created: Created
    last_modified: LastModified


class BookBody(BaseModel):
    book: OLBook


class BookSaved(BaseModel):
    book: OLBook
    id: str = Field(default_factory=uuid.uuid4, alias="_id")
