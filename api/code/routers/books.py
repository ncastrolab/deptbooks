from typing import List

from fastapi import APIRouter, Depends, Query, Body
from starlette import status

from .queries import BooksGenericQuery
from ..connections.open_library_client import OpenLibraryClient
from ..models.book import BookSaved, OLBook
from ..repositories.books import OpenLibraryBooksRepository
from ..services.books_service import BooksService

books_router = APIRouter(prefix="/books")


@books_router.get("/all", tags=["books"], response_model=List[BookSaved])
def get_all():
    return BooksService(OpenLibraryClient(), OpenLibraryBooksRepository).get_all()


@books_router.get("/ISBN/{isbn}", tags=["books"])
def get_by_isbn(isbn: str = Query(regex="1234123412 | 123412341X", default="9780140328721")):
    return BooksService(OpenLibraryClient(), OpenLibraryBooksRepository).get_by_isbn(isbn)


@books_router.get("/", tags=["books"])
def get_by_generic_query(query: BooksGenericQuery = Depends()):
    return BooksService(OpenLibraryClient(), OpenLibraryBooksRepository).get_by_generic_query(query)


@books_router.get("/external_id/{id_}", tags=["books"])
def get_by_external_id(id_: str):
    return BooksService(OpenLibraryClient(), OpenLibraryBooksRepository).get_by_external_id(id_)


@books_router.post("/", response_description="Create a new book",
                   status_code=status.HTTP_201_CREATED, tags=["books"])
def save_book(book: OLBook = Body(...)):
    return BooksService(OpenLibraryClient(), OpenLibraryBooksRepository).save_book(book)
