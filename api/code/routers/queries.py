from pydantic import BaseModel


class BooksGenericQuery(BaseModel):
    title: str | None = None
    author: str | None = None
    subject: str | None = None
    place: str | None = None
    person: str | None = None
    language: str | None = None
    publisher: str | None = None
    page: int = 1
    limit: int = 20
