from fastapi import APIRouter, Body
from starlette import status

from ..models.comment import CommentSave, CommentUpdate, CommentSaved
from ..services.comments_service import CommentsService

comments_router = APIRouter(prefix="/books")


@comments_router.get("/{book_id}/comments/{id}", tags=["comments"])
def get_single_comment(book_id, id):
    return CommentsService.get_by_id(id)


@comments_router.get("/{book_id}/comments", tags=["comments"])
def get_all_for_book(book_id):
    return CommentsService.get_all_for_book(book_id)


@comments_router.post("/{book_id}/comments", response_description="Save comment",
                      status_code=status.HTTP_201_CREATED, response_model=CommentSaved, tags=["comments"])
def save_book(book_id: str, comment: CommentUpdate = Body(...)):
    return CommentsService.save_comment(book_id, comment)


@comments_router.put("/{book_id}/comments/{comment_id}", response_description="Update comment",
                     status_code=status.HTTP_201_CREATED, response_model=CommentSave, tags=["comments"])
def update_comment(comment_id: str, book_id: str, comment: CommentUpdate = Body(...)):
    return CommentsService.update_comment(comment_id, book_id, comment)


@comments_router.delete("/{book_id}/comments/{id}", status_code=status.HTTP_204_NO_CONTENT, tags=["comments"])
def delete_comment(book_id, id_):
    CommentsService.delete(id_)
    return {}
