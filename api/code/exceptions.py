class BooksRepositoryException(Exception):
    pass


class CommentDoesNotExist(Exception):
    pass


class FetchBookByIsbnError(Exception):
    pass


class BookDoesNotExistException(Exception):
    pass
