import os

from pymongo import mongo_client

client = mongo_client.MongoClient(os.environ.get("DB_URL"))
print('Connected to MongoDB...')

db = client["books_collections"]
Books = db.books
Comments = db.comments
