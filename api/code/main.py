from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi_pagination import add_pagination
from starlette.responses import RedirectResponse, JSONResponse

from .routers import books as books
from .routers import comments as comments

load_dotenv()

description = """
Dept books interview project. 🚀

## Books

You can:

* **Get Book by isbn**.
* **Get Book by Open Library id (for now is our books provider)
* **Get list of works, inside each work you can find books open library ids**
* **Save a book


## Comments

You can make CRUD operations


Let's see if i get the job

"""


app = FastAPI(
    title="DeptBooks",
    description=description,
    version="0.0.1"
)

add_pagination(app)


@app.get("/", include_in_schema=False)
def redirect_to_docs():
    return RedirectResponse(url='/docs/')


app.include_router(books.books_router)
app.include_router(comments.comments_router)


@app.exception_handler(Exception)
async def unicorn_exception_handler(_, exc: Exception):
    return JSONResponse(
        status_code=422,
        content={"message": f"Oops! {str(exc)} did something. There goes a rainbow..."},
    )