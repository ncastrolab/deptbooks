import httpx

from ..exceptions import FetchBookByIsbnError
from ..models.book import BookSaved, BookBody


class BooksService:

    def __init__(self, external_client, db_repository):
        self.external_client = external_client
        self.db_repository = db_repository

    def get_by_isbn(self, isbn):
        try:
            saved_book = self.db_repository.retrieve_by_isbn(isbn)
            if saved_book is not None:
                book = saved_book
            else:
                book = BookBody(book=self.external_client.get_by_isbn(isbn))
        except httpx.HTTPError:
            raise FetchBookByIsbnError()
        else:
            return book

    def get_by_generic_query(self, query):
        try:
            book = self.external_client.get_by_generic_query(**query.dict(exclude_none=True))
        except httpx.HTTPError:
            raise FetchBookByIsbnError()
        else:
            return book

    def save_book(self, book):
        return self.db_repository.save(BookSaved(book=book))

    def get_by_external_id(self, client_id):
        try:
            saved_book = self.db_repository.retrieve_by_external_id(client_id)
            if saved_book is not None:
                book = saved_book
            else:
                book = BookBody(book=self.external_client.get_by_external_id(client_id))
        except httpx.HTTPError:
            raise FetchBookByIsbnError()
        else:
            return book

    def get_all(self):
        return self.db_repository.get_all()
