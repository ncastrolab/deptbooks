from ..exceptions import BookDoesNotExistException
from ..models.comment import CommentSave, CommentSaved
from ..repositories.books import BooksRepository, OpenLibraryBooksRepository
from ..repositories.comments import CommentsRepository


class CommentsService:

    @staticmethod
    def get_by_id(id_):
        return CommentsRepository.get_by_id(id_)

    @staticmethod
    def get_all_for_book(book_id):
        return CommentsRepository.get_all_for_book(book_id)

    @staticmethod
    def save_comment(book_id, comment):
        book = OpenLibraryBooksRepository.get_by_id(book_id)
        if book.id is not None:
            saved_comment = CommentsRepository.save(CommentSaved(**dict(comment), book_id=book_id))
            return saved_comment
        else:
            raise BookDoesNotExistException()

    @staticmethod
    def update_comment(comment_id, book_id, comment):
        book = BooksRepository.get_by_id(book_id)
        if book.id is not None:
            return CommentsRepository.update(comment_id, book_id, comment)
        else:
            raise BookDoesNotExistException()

    @staticmethod
    def delete(id_):
        return CommentsRepository.delete(id_)
