from abc import abstractmethod, ABC

from fastapi.encoders import jsonable_encoder
from pymongo.results import InsertOneResult

from ..database import Books
from ..models.book import BookSaved


class BooksRepository(ABC):

    @staticmethod
    @abstractmethod
    def save(book: BookSaved):
        pass

    @staticmethod
    @abstractmethod
    def get_by_id(book_id):
        pass

    @staticmethod
    @abstractmethod
    def retrieve_by_isbn(isbn):
        pass

    @staticmethod
    @abstractmethod
    def retrieve_by_external_id(external_id):
        pass

    @staticmethod
    @abstractmethod
    def get_all():
        pass


class OpenLibraryBooksRepository(BooksRepository):

    @staticmethod
    def save(book: BookSaved):
        already_exist = Books.find_one({"book.key": book.book.key})
        if already_exist is not None:
            book = already_exist
        else:
            insert_one_result: InsertOneResult = Books.insert_one(jsonable_encoder(book))
            book.id = str(insert_one_result.inserted_id)
        return book

    @staticmethod
    def retrieve_by_isbn(isbn):
        return Books.find_one({"book.isbn_10": isbn})

    @staticmethod
    def retrieve_by_external_id(external_id):
        return Books.find_one({"book.key": f"books/{external_id}"})

    @staticmethod
    def get_by_id(book_id):
        return BookSaved(**Books.find_one({"_id": book_id}))

    @staticmethod
    def get_all():
        return list(Books.find({}))
