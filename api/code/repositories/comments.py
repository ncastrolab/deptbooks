from fastapi.encoders import jsonable_encoder
from pymongo.results import InsertOneResult

from ..database import Comments
from ..exceptions import CommentDoesNotExist
from ..models.comment import CommentSave, CommentSaved, CommentUpdate


class CommentsRepository:

    @staticmethod
    def save(comment: CommentSaved):
        insert_one_result: InsertOneResult = Comments.insert_one(jsonable_encoder(comment))
        comment.id = str(insert_one_result.inserted_id)
        return comment

    @staticmethod
    def get_by_id(id_):
        comment = Comments.find_one({"_id": id_})
        if comment is None:
            raise CommentDoesNotExist()

    @staticmethod
    def get_all_for_book(book_id):
        return list(Comments.find({"book_id": book_id}))

    @staticmethod
    def delete(id_):
        delete_result = Comments.delete_one({"_id": id_})
        if delete_result.deleted_count == 1:
            return delete_result
        else:
            raise CommentDoesNotExist()

    @staticmethod
    def update(comment_id, book_id, comment: CommentUpdate):
        update_result = Comments.update_one(
            {"_id": comment_id}, {"$set": comment.dict()}
        )
        if update_result.modified_count == 1:
            return CommentSave(id=comment_id, book_id=book_id, **comment.dict())
        else:
            raise CommentDoesNotExist()
