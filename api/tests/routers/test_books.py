import json
from unittest.mock import patch

import httpx
import pytest
from starlette.testclient import TestClient

from api.code.main import app
from api.code.models.book import BookSaved
from api.tests.test_data import *


@pytest.fixture
def client():
    with TestClient(app) as c:
        yield c


def load_json(relative_path):
    import os
    script_dir = os.path.dirname(__file__)
    rel_path = relative_path
    abs_file_path = os.path.join(script_dir, rel_path)
    f = open(abs_file_path)
    return json.load(f)


@pytest.mark.respx(base_url="https://openlibrary.org")
def test_get_book_by_isbn_return_book_from_external_api(client, respx_mock):
    isbn_call_mock = respx_mock.get(
        "isbn/9780140328721.json"
    ).mock(
        return_value=httpx.Response(200, json=load_json("book.json"))
    )
    with patch('api.code.routers.books.OpenLibraryBooksRepository') as mocked_repository:
        mocked_repository.retrieve_by_isbn.return_value = None
        response = client.get("/books/ISBN/9780140328721")
        assert response.status_code == 200
        assert isbn_call_mock.called
        assert response.json() == dict(book=load_json("book.json"))


@pytest.mark.respx(base_url="https://openlibrary.org")
def test_get_book_by_ol_id_return_book_from_external_api(client, respx_mock):
    external_call_mock = respx_mock.get(
        "books/OL7353617M.json"
    ).mock(
        return_value=httpx.Response(200, json=load_json("book.json"))
    )
    with patch('api.code.routers.books.OpenLibraryBooksRepository') as mocked_repository:
        mocked_repository.retrieve_by_external_id.return_value = None
        response = client.get("/books/external_id/OL7353617M")
        assert response.status_code == 200
        assert external_call_mock.called
        assert response.json() == dict(book=load_json("book.json"))


@pytest.mark.respx(base_url="https://openlibrary.org", assert_all_called=False)
def test_get_book_by_isbn_when_book_is_saved_return_the_one_saved(client, respx_mock):
    isbn_call_mock = respx_mock.get(
        "isbn/9780140328721.json"
    ).mock(
        return_value=httpx.Response(200, json=load_json("book.json"))
    )
    with patch('api.code.routers.books.OpenLibraryBooksRepository') as mocked_repository:
        saved_book = BookSaved(book=load_json("book.json"), _id="066de609-b04a-4b30-b46c-32537c7f1f6e")

        mocked_repository.retrieve_by_isbn.return_value = saved_book

        response = client.get("/books/ISBN/9780140328721")
        assert response.status_code == 200
        assert not isbn_call_mock.called

        assert response.json() == dict(book=load_json("book.json"), _id="066de609-b04a-4b30-b46c-32537c7f1f6e")


@pytest.mark.respx(base_url="https://openlibrary.org/")
def test_search_books_lists(client, respx_mock):
    books_repository_mock = respx_mock.get(
        "search.json/?subject=travel&place=istanbul&page=1&limit=20"
    ).mock(
        return_value=httpx.Response(200, json=search_query_result)
    )
    response = client.get("/books?place=istanbul&subject=travel&page=1&limit=20")
    assert response.status_code == 200
    assert books_repository_mock.called
    assert response.json() == search_query_result


def test_save_book(client):
    with patch("api.code.routers.books.BooksService") as mocked_service:
        saved_book = BookSaved(book=load_json("book.json"), _id="066de609-b04a-4b30-b46c-32537c7f1f6e")
        service_instance = mocked_service.return_value
        service_instance.save_book.return_value = saved_book
        response = client.post("/books", json=load_json("book.json"))
        service_instance.save_book.assert_called_with(load_json("book.json"))
        assert response.status_code == 201
        assert response.json() == dict(book=load_json("book.json"), _id="066de609-b04a-4b30-b46c-32537c7f1f6e")


def test_save_book_with_wrong_format_return_error(client):
    response = client.post("/books", json={"some": "json"})

    assert response.status_code == 422
