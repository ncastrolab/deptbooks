
search_query_result = repr({
    "numFound": 76,
    "start": 0,
    "numFoundExact": "true",
    "docs": [
        {
            "key": "/works/OL14860424W",
            "type": "work",
            "seed": [
                "/books/OL5928213M",
                "/books/OL15046049M",
                "/books/OL5981438M",
                "/books/OL5662140M",
                "/books/OL22349316M",
                "/books/OL17763659M",
                "/books/OL17764833M",
                "/books/OL4133854M",
                "/books/OL3269927M",
                "/books/OL2980196M",
                "/books/OL17730524M",
                "/books/OL2097846M",
                "/books/OL2731262M",
                "/books/OL1646511M",
                "/books/OL1482488M",
                "/books/OL18381751M",
                "/works/OL14860424W",
                "/subjects/chronology",
                "/subjects/social_life_and_customs",
                "/subjects/spanish_fiction",
                "/subjects/argentine_fiction",
                "/subjects/translations_into_english",
                "/subjects/manners_and_customs",
                "/subjects/fiction",
                "/subjects/fiction_general",
                "/subjects/person:julio_cort\u00e1zar",
                "/authors/OL20988A"
            ],
            "title": "Rayuela",
            "title_suggest": "Rayuela",
            "edition_count": 16,
            "edition_key": [
                "OL5928213M",
                "OL15046049M",
                "OL5981438M",
                "OL5662140M",
                "OL22349316M",
                "OL17763659M",
                "OL17764833M",
                "OL4133854M",
                "OL3269927M",
                "OL2980196M",
                "OL17730524M",
                "OL2097846M",
                "OL2731262M",
                "OL1646511M",
                "OL1482488M",
                "OL18381751M"
            ],
            "publish_date": [
                "1987",
                "1984",
                "1986",
                "1963",
                "1972",
                "1980",
                "1970",
                "1991",
                "1979",
                "1966",
                "1992",
                "1967",
                "1993"
            ],
            "publish_year": [
                1987,
                1984,
                1986,
                1963,
                1972,
                1980,
                1970,
                1991,
                1979,
                1966,
                1992,
                1967,
                1993
            ],
            "first_publish_year": 1963,
            "number_of_pages_median": 635,
            "lccn": [
                "86025347",
                "91203050",
                "83191943",
                "68110723",
                "80111459",
                "84230287",
                "88144504",
                "93150303",
                "64050251",
                "66010409"
            ],
            "publish_place": [
                "London",
                "Nanterre, France",
                "Buenos Aires",
                "Barcelona : Bruguera, 1979",
                "Barcelona (Espan\u0303a)",
                "Caracas, Venezuela",
                "Madrid",
                "Barcelona",
                "New York",
                "Moskva"
            ],
            "oclc": [
                "11129972",
                "34347687",
                "3336757"
            ],
            "contributor": [
                "Yurkie\u0301vich, Sau\u0301l.",
                "Alazraki, Jaime.",
                "Amoro\u0301s, Andre\u0301s.",
                "Ortega, Julio, 1942-"
            ],
            "lcc": [
                "PQ-7797.00000000.C7145 R313 1987",
                "PQ-7797.00000000.C7145 R3 1992",
                "PZ-0003.00000000.C81929 Ho3",
                "PQ-7797.00000000.C7145 xR3 1977",
                "PQ-7797.00000000.C7145R313",
                "PZ-0003.00000000.C81929 Ho",
                "PQ-7797.00000000.C7145 R3 1984",
                "PQ-7797.00000000.C7145 R3 1991",
                "PQ-7797.00000000.C7145 R3 1980",
                "PQ-7797.00000000.C7145 R3",
                "PQ-7797.00000000.C7145 R317x 1986",
                "PQ-7797.00000000.C7145 R3 1993"
            ],
            "ddc": [
                "863"
            ],
            "isbn": [
                "8402067409",
                "8435001458",
                "9789505571338",
                "8432221554",
                "9788435001458",
                "9788402067401",
                "9788420421520",
                "8400071123",
                "8420421529",
                "9788432221552",
                "9780394752846",
                "8466000518",
                "9788400071127",
                "9788466000512",
                "9788437604572",
                "846600050X",
                "950557133X",
                "8437604575",
                "9788466000505",
                "0394752848"
            ],
            "last_modified_i": 1675358672,
            "ebook_count_i": 3,
            "ebook_access": "borrowable",
            "has_fulltext": "true",
            "public_scan_b": "false",
            "ia": [
                "rayuela0000cort_z0n7",
                "rayuela0000cort_s0j4",
                "hopscotch00cort",
                "hopscotch0000cort"
            ],
            "ia_collection": [
                "binghamton-ol",
                "china",
                "dartmouthlibrary-ol",
                "gwulibraries-ol",
                "inlibrary",
                "internetarchivebooks",
                "johnshopkins-ol",
                "kalamazoocollege-ol",
                "printdisabled",
                "riceuniversity-ol",
                "rochester-ol",
                "universityofcoloradoboulder-ol",
                "wrlc-ol"
            ],
            "ia_collection_s": "binghamton-ol;china;dartmouthlibrary-ol;gwulibraries-ol;inlibrary;internetarchivebooks;johnshopkins-ol;kalamazoocollege-ol;printdisabled;riceuniversity-ol;rochester-ol;universityofcoloradoboulder-ol;wrlc-ol",
            "lending_edition_s": "OL17764833M",
            "lending_identifier_s": "rayuela0000cort_z0n7",
            "printdisabled_s": "OL17764833M;OL2980196M;OL2731262M",
            "cover_edition_key": "OL2980196M",
            "cover_i": 1047466,
            "publisher": [
                "Sudamericana",
                "ALLCA XXe",
                "Biblioteca Ayacucho",
                "EDHASA",
                "Pantheon Books",
                "Collins, Harvill P.",
                "Seix Barral",
                "Ediciones Alfaguara",
                "ALLCA XXe, Universite\u0301 de Paris X, Centre de recherches latino-ame\u0301ricaines",
                "\"Khudozh. Lit-ra\"",
                "Ca\u0301tedra",
                "Editorial Sudamericana"
            ],
            "language": [
                "spa",
                "eng",
                "rus"
            ],
            "author_key": [
                "OL20988A"
            ],
            "author_name": [
                "Julio Corta\u0301zar"
            ],
            "person": [
                "Julio Cort\u00e1zar"
            ],
            "subject": [
                "Chronology",
                "Social life and customs",
                "Spanish fiction",
                "Argentine fiction",
                "Translations into English",
                "Manners and customs",
                "Fiction",
                "Fiction, general"
            ],
            "id_goodreads": [
                "46171",
                "1013798",
                "4888352",
                "6929549",
                "564043",
                "4908034",
                "53413",
                "6929550"
            ],
            "id_librarything": [
                "57814"
            ],
            "ia_box_id": [
                "IA157801",
                "IA40248717",
                "IA40303820"
            ],
            "publisher_facet": [
                "\"Khudozh. Lit-ra\"",
                "ALLCA XXe",
                "ALLCA XXe, Universite\u0301 de Paris X, Centre de recherches latino-ame\u0301ricaines",
                "Biblioteca Ayacucho",
                "Ca\u0301tedra",
                "Collins, Harvill P.",
                "EDHASA",
                "Ediciones Alfaguara",
                "Editorial Sudamericana",
                "Pantheon Books",
                "Seix Barral",
                "Sudamericana"
            ],
            "person_key": [
                "julio_cort\u00e1zar"
            ],
            "person_facet": [
                "Julio Cort\u00e1zar"
            ],
            "subject_facet": [
                "Argentine fiction",
                "Chronology",
                "Fiction",
                "Fiction, general",
                "Manners and customs",
                "Social life and customs",
                "Spanish fiction",
                "Translations into English"
            ],
            "_version_": 1756740899485777925,
            "lcc_sort": "PQ-7797.00000000.C7145 R317x 1986",
            "author_facet": [
                "OL20988A Julio Corta\u0301zar"
            ],
            "subject_key": [
                "argentine_fiction",
                "chronology",
                "fiction",
                "fiction_general",
                "manners_and_customs",
                "social_life_and_customs",
                "spanish_fiction",
                "translations_into_english"
            ],
            "ddc_sort": "863"
        }
    ],
    "num_found": 76,
    "q": "",
    "offset": "null"
})