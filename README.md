# deptbooks

Este proyecto es mi intento de solucion del ejercicio de entrevista sobre libros. Hay que tener en cuenta que es la primera vez que trabajo con fastApi, por ahi sirve para tener un poco de contexto en algunas convenciones de nombres o decicioes que he tomado


Para correr el proyecto
`docker-compose up`
para acceder desde el navegaro a la documentacion
http://locahost:8000

Arme el proyecto no solo para resolver el enunciado sino tambien para mostra un poco lo que se (entiendo que eso sirve para una entrevista).
Los test solo cubren la parte de los `books` pero la parte de `comments` 
no esta cubierta por una cuestion de tiempo y ademas use los test para ir construyendo la app y al momento de llegar a los comentarios no lo vi necesario.

Con respecto al ultimo punto del enunciado espero haberlo cumplido. Basicamente cree un cliente que respeta una interfaz heredada y un repositorio para la db que tiene la misma estructura
si se quiere implementar otro proveedor de libros se deberian cambiar esas dos clases por otras y ademas se deberia agregar la validacion de un nuevo posible formato en el endpoint de guardado (para que no enviend cualquier cosa)
y entiendo que todo seguiria funcionando igual.

Pendientes:
* Si se actualiza un comentario con el mismo texto que ya existe, se lanza un error porque figura como que no se actualizo nada. Es la manera en la que funciona mongo (y esta mas que bien) pero ahi deberia haber implementado un workarround, sinceramente me parecio invertir tiempo en algo que no da valor
* Manejos de errores con responses bien descriptivos. En general me gusta mucho lo que en Flask se le llama `error_handlers` que son los manejaroes de errores para las respones. Me faltaria en esta implementacion tener codigos de error en las excepcione y error handlers especificos por excepcion entonces podria devolver unas bonitas respuestas de error sin el codigo de la app se entere
* No pude implementar el save solo si no existe de manera correcta en mongodb, por alguna razon lanza ese error
```
changed: UpdateResult = Books.update_one(
    {"book.key": book.book.key},
    {
        "$setOnInsert": jsonable_encoder(book),
        "$set": {"book": jsonable_encoder(book.book)},
    },
    upsert=True,
)
pymongo.errors.WriteError: Updating the path 'book' would create a conflict at 'book', full error: {'index': 0, 'code': 40, 'errmsg': "Updating the path 'book' would create a conflict at 'book'"}
```
* No llegue a documentar swagger completamente, espero que no les cueste mucho seguir la documentacion
* Me di cuenta un poco tarde que no habia manera de obtener todos los libros guardados, implemente un getall pero no llegue a agregarle paginacion