Crear una API de gestión de libros y comentarios sobre los mismos. 
No se requiere implementar usuarios ni permisos y, en lo posible, hacer la implementación usando Flask o FastAPI (aunque no es excluyente).

Para proveedor de la información sobre los libros, se puede usar https://openlibrary.org/dev/docs/api/books

    Nota: Podes pensarlo como que ellos son tu fuente, de qué libros hay disponibles, de donde vos te alimentas de los datos; aunque podría ser cualquier otra API.


Esta API (tu api) deberá permitir lo siguiente:
- Obtener información de un libro por ISBN
- 
- Guardar información de un libro
- 
- Obtener una lista de libros
- 
- Permitir operaciones CRUD sobre comentarios en un libro.
- 
- Dejar abierta la posibilidad de intercambiar openlibrary por cualquier otra API (parametrización del proveedor de la info sobre los libros)
